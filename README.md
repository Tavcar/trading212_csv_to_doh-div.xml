# Readme!

Skripta ki generira xml obrazec za napoved dohodnine od dividend.
Podatki se preberejo iz CSV datoteke ki jo dobite ko naredite history export za prejšnje leto.

Velik del te skripte je prevzet iz <https://github.com/jamsix/ib-edavki> 



# Trading 212 -> FURS eDavki konverter
_Skripta prevede CSV poročilo iz platforme Trading 212 v XML format primeren za uvoz v <https://edavki.durs.si/>_

Zaenkrat je podprt samo  Doh-Div obrazec (Napoved za odmero dohodnine od dividend) 

Poleg pretvorbe vrednosti skripta naredi še konverzijo iz tujih valut v EUR po tečaju Banke Slovenije na dan posla.


# HOW TO:
0. Predvidevam da imaste python nameščen in da veste poganjati python skripte
1. `python t212_edavki.py`   Ta ukaz bo generiral 2 xml fajla:

    * taxpayer.xml   V tem fajlu vpišite svoje osebne podatke.
    * bsrate-[date].xml   Ta fajl vsebuje informacije o konverziji tujih valut v EUR. 
2. Kopirajte CSV fajl ki ste ga izvozili iz Trading 212 v ta repository (zraven `t212_edavki.py`) in ga preimenujte v `dividends.csv`.
3. Po ponovnem zagonu `python t212_edavki.py` vas bo skripta obvestila o manjkajočih podatkih o ticker simbolih. Manjkajoče informacije dopišite v `companies.xml`. Primer; hočemo dodati VUSA ticker symbol: 

    Before:
    ```
    <companies>
        <company>
            <conid>265598</conid>
            <symbol>AAPL</symbol>
            <name>Apple Inc</name>
            <taxNumber>942404110</taxNumber>
            <address>1 Infinite Loop, Cupertino, CA 95014</address>
            <country>US</country>
        </company>
        ...
    </companies>
    ```
    After:
    ```
    <companies>
        <company>
            <conid>IE00B3XXRP09</conid>
            <symbol>VUSA</symbol>
            <name>Vanguard S&amp;P 500 UCITS ETF</name>
            <taxNumber>IE00B3XXRP09</taxNumber>
            <address>Vanguard Group (Ireland) Ltd 70 Sir John Rogerson's Quay Dublin 2 Ireland</address>
            <country>IE</country>
        </company>
        <company>
            <conid>265598</conid>
            <symbol>AAPL</symbol>
            <name>Apple Inc</name>
            <taxNumber>942404110</taxNumber>
            <address>1 Infinite Loop, Cupertino, CA 95014</address>
            <country>US</country>
        </company>
        ...
    </companies>
    ```
4. Ponavljajte 3. korak dokler ne dobite `Doh-Div.xml` fajl.
