from distutils.core import setup

with open("README.md", "r") as fh:
    long_description = fh.read()

setup(
    name="t212_edavki",
    version="0.0.1",
    py_modules=["t212_edavki"],
    python_requires=">=3",
    entry_points={
        "console_scripts": ["t212_edavki=t212_edavki:main"]
    },
    author="Marko Tavcar",
    author_email="livadic.cvijetko@gmail.com",
    url='https://gitlab.com/Tavcar/trading212_csv_to_doh-div.xml',
)