import os
import glob
import csv
from xml.dom import minidom
import urllib.request
import datetime
import xml.etree.ElementTree as etree

test=False
reportYear=2021
testYearDiff=1
rates = {}
relief_statements = {}
taxpayerConfig = []
envelope = None
dYear = reportYear
body = None

def _get_rates():
    """ Creating daily exchange rates object """
    global rates
    bsRateXmlUrl = "https://www.bsi.si/_data/tecajnice/dtecbs-l.xml"
    bsRateXmlFilename = (
        "bsrate-"
        + str(datetime.date.today().year)
        + str(datetime.date.today().month)
        + str(datetime.date.today().day)
        + ".xml"
    )
    if not os.path.isfile(bsRateXmlFilename):
        for file in glob.glob("bsrate-*.xml"):
            os.remove(file)
        urllib.request.urlretrieve(bsRateXmlUrl, bsRateXmlFilename)
    bsRateXml = etree.parse(bsRateXmlFilename).getroot()

    rates = {}
    for d in bsRateXml:
        date = d.attrib["datum"].replace("-", "")
        rates[date] = {}
        for r in d:
            currency = r.attrib["oznaka"]
            rates[date][currency] = r.text

def _get_relief_statements():
    global relief_statements
    url = "https://raw.githubusercontent.com/jamsix/ib-edavki/master/relief-statements.xml"
    filename = "relief_statements.xml"
    if not os.path.isfile(filename):
        urllib.request.urlretrieve(url, filename)
    rates_xml = etree.parse(filename).getroot()
    relief_statements = {}

    for statement in rates_xml:
        relief_statements[statement.find("country").text] = statement.find("statement").text
    

def _read_taxpayerxml():
    global taxpayerConfig
    if not os.path.isfile("taxpayer.xml"):
        print("Modify taxpayer.xml and add your data first!")
        f = open("taxpayer.xml", "w+", encoding="utf-8")
        f.write(
            "<taxpayer>\n"
            "   <taxNumber>12345678</taxNumber>\n"
            "   <taxpayerType>FO</taxpayerType>\n"
            "   <name>Janez Novak</name>\n"
            "   <address1>Slovenska 1</address1>\n"
            "   <city>Ljubljana</city>\n"
            "   <postNumber>1000</postNumber>\n"
            "   <postName>Ljubljana</postName>\n"
            "   <email>janez.novak@furs.si</email>\n"
            "   <telephoneNumber>01 123 45 67</telephoneNumber>\n"
            "   <residentCountry>SI</residentCountry>\n"
            "   <isResident>true</isResident>\n"
            "</taxpayer>"
        )
        exit(0)
    taxpayer = etree.parse("taxpayer.xml").getroot()
    taxpayerConfig = {
        "taxNumber": taxpayer.find("taxNumber").text,
        "taxpayerType": "FO",
        "name": taxpayer.find("name").text,
        "address1": taxpayer.find("address1").text,
        "city": taxpayer.find("city").text,
        "postNumber": taxpayer.find("postNumber").text,
        "postName": taxpayer.find("postName").text,
        "email": taxpayer.find("email").text,
        "telephoneNumber": taxpayer.find("telephoneNumber").text,
        "residentCountry": taxpayer.find("residentCountry").text,
        "isResident": taxpayer.find("isResident").text,
    }

def _create_envelope():
    global envelope, dYear, body
    envelope = etree.Element("Envelope", xmlns="http://edavki.durs.si/Documents/Schemas/Doh_Div_3.xsd")      
    envelope.set("xmlns:edp", "http://edavki.durs.si/Documents/Schemas/EDP-Common-1.xsd")

    header = etree.SubElement(envelope, "edp:Header")
    taxpayer = etree.SubElement(header, "edp:taxpayer")
    etree.SubElement(taxpayer, "edp:taxNumber").text = taxpayerConfig["taxNumber"]
    etree.SubElement(taxpayer, "edp:taxpayerType").text = taxpayerConfig["taxpayerType"]
    etree.SubElement(taxpayer, "edp:name").text = taxpayerConfig["name"]
    etree.SubElement(taxpayer, "edp:address1").text = taxpayerConfig["address1"]
    etree.SubElement(taxpayer, "edp:city").text = taxpayerConfig["city"]
    etree.SubElement(taxpayer, "edp:postNumber").text = taxpayerConfig["postNumber"]
    etree.SubElement(taxpayer, "edp:postName").text = taxpayerConfig["postName"]

    workflow = etree.SubElement(header, "edp:Workflow")
    if test == True:
        etree.SubElement(workflow, "edp:DocumentWorkflowID").text = "I"
    else:
        etree.SubElement(workflow, "edp:DocumentWorkflowID").text = "O"
    etree.SubElement(envelope, "edp:AttachmentList")
    etree.SubElement(envelope, "edp:Signatures")
    body = etree.SubElement(envelope, "body")
    Doh_Div = etree.SubElement(body, "Doh_Div")
    if test == True:
        dYear = str(reportYear + testYearDiff)
    else:
        dYear = str(reportYear)
    etree.SubElement(Doh_Div, "Period").text = dYear
    etree.SubElement(Doh_Div, "EmailAddress").text = taxpayerConfig["email"]
    etree.SubElement(Doh_Div, "PhoneNumber").text = taxpayerConfig["telephoneNumber"]
    etree.SubElement(Doh_Div, "ResidentCountry").text = taxpayerConfig["residentCountry"]
    etree.SubElement(Doh_Div, "IsResident").text = taxpayerConfig["isResident"]

def _convert_to_eur(dividends):
    _convert_to_eur_helper(dividends, "currency", "amount", "amountEUR")
    _convert_to_eur_helper(dividends, "taxCurrency", "tax", "taxEUR")

def _convert_to_eur_helper(dividends, curency_field, amount_field, amount_eur_field):
    for dividend in dividends:
        if dividend[curency_field] != 'EUR':
            date = dividend["dateTime"][0:8]
            currency = dividend[curency_field]
            if currency == 'GBX':
                currency = 'GBP'
                dividend[amount_field] /= 100.00

            if date in rates and currency in rates[date]:
                rate = float(rates[date][currency])
            else:
                for i in range(0, 7):
                    date = str(int(date) - 1)#TODO: this can be problematic
                    if date in rates and currency in rates[date]:
                        rate = float(rates[date][currency])
                        print(f"There is no exchange rate for {currency} on {dividend['dateTime'][0:8]}, using {date}")
                        break
                    if i == 6:
                        print(f"Error: There is no exchange rate for {currency} on {str(date)}")
                        exit(0)
            dividend[amount_eur_field] = dividend[amount_field] / rate
            dividend[curency_field] = 'EUR'
        else:
            dividend[amount_eur_field] = dividend[amount_field]



def _read_dividends_from_csv():
    dividends = []

    try:
        with open('dividends.csv', 'r') as csv_file:
            for row in csv.DictReader(csv_file):
                if "Dividend" not in row['Action']:
                    continue
                try:
                    _tax = float(row['Withholding tax'])
                except Exception as e:
                    _tax = 0.0
                    print(e)
                dividends.append({
                    "currency": row["Currency (Price / share)"],
                    "amount": float(row["Price / share"]) * float(row["No. of shares"]),
                    "taxCurrency": row["Currency (Withholding tax)"],
                    "tax": _tax,
                    "dateTime": row["Time"].replace('-', ''),
                    "symbol": row["Ticker"]
                })
            csv_file.close()
    except Exception as e:
        print(e)
        print("Missing dividends.csv file")
        exit(1)
    return dividends




def _fill_company_information(dividends):
    global relief_statements
    filename = "companies.xml"
    url = "https://raw.githubusercontent.com/jamsix/ib-edavki/master/companies.xml"
    if not os.path.isfile(filename):
        urllib.request.urlretrieve(url, filename)
    companies = etree.parse(filename).getroot()
    for div in dividends:
        for co in companies:
            if co.find("symbol").text == div['symbol']:
                div['description'] = co.find("name").text
                div['address'] = co.find("address").text
                div['taxNumber'] = co.find("taxNumber").text
                div['country'] = co.find("country").text
                div['reliefStatement'] = relief_statements.get(div['country'])
                break
        if not div.get('description') and not div.get('address'):
            print(f"Missing company {div['symbol']} in {filename}")
            exit(1)
        


_get_rates()
_read_taxpayerxml()
_create_envelope()
_get_relief_statements()

dividends = _read_dividends_from_csv()
_fill_company_information(dividends)
_convert_to_eur(dividends)




for dividend in dividends:
    if round(dividend["amountEUR"], 2) <= 0:
        continue
    Dividend = etree.SubElement(body, "Dividend")
    etree.SubElement(Dividend, "Date").text = (
        dYear + "-" + dividend["dateTime"][4:6] + "-" + dividend["dateTime"][6:8]
    )
    if "taxNumber" in dividend:
        etree.SubElement(Dividend, "PayerIdentificationNumber").text = dividend["taxNumber"]

    if "description" in dividend:
        etree.SubElement(Dividend, "PayerName").text = dividend["description"]
    elif "symbol" in dividend:
        etree.SubElement(Dividend, "PayerName").text = dividend["symbol"]

    if "address" in dividend:
        etree.SubElement(Dividend, "PayerAddress").text = dividend["address"]

    if "country" in dividend:
        etree.SubElement(Dividend, "PayerCountry").text = dividend["country"]

    etree.SubElement(Dividend, "Type").text = "1"
    etree.SubElement(Dividend, "Value").text = "{0:.2f}".format(dividend["amountEUR"] + dividend["taxEUR"])
    etree.SubElement(Dividend, "ForeignTax").text = "{0:.2f}".format(dividend["taxEUR"])

    if "country" in dividend:
        etree.SubElement(Dividend, "SourceCountry").text = dividend["country"]

    if "reliefStatement" in dividend:
        etree.SubElement(Dividend, "ReliefStatement").text = dividend["reliefStatement"]
    else:
        etree.SubElement(Dividend, "ReliefStatement").text = ""

xmlString = etree.tostring(envelope)
prettyXmlString = minidom.parseString(xmlString).toprettyxml(indent="\t")
with open("Doh-Div.xml", "w", encoding="utf-8") as f:
    f.write(prettyXmlString)
    print("Doh-Div.xml created")
